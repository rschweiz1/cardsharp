from objects import Dealer
from hand_eval import evaluate_hand

dealer = Dealer()
dealer.shuffle_deck()
dealer.deal_players(2, 9)
print(dealer.hands)

dealer.deal_board(5)
print(dealer.board)

winner = dealer.determine_winner()

print('Player {} wins with hand: {}!'.format(winner, evaluate_hand(dealer.board + dealer.hands[winner])[0]))

from objects import Dealer, Player
from itertools import combinations
import json

def generate_equity_table():
    dealer = Dealer()
    equities_twoway = {}
    equities_threeway = {}
    combos = list(combinations(dealer.deck, 2))
    for i in range(len(combos)):
        dealer.hands = [list(combos[i])]
        hand_short = dealer.hands[0][0][0]+dealer.hands[0][1][0]
        if dealer.hands[0][0][1] == dealer.hands[0][1][1]:
            hand_short += 's'
        if hand_short not in equities_twoway:
            for i in range(2):
                dealer.deck.remove(dealer.hands[0][i])
            equity_twoway = dealer.blind_equity(0,2)
            equity_threeway = dealer.blind_equity(0,3)
            for i in range(2):
                dealer.deck.append(dealer.hands[0][i])
            equities_twoway[hand_short] = round(equity_twoway, 4)
            equities_threeway[hand_short] = round(equity_threeway, 4)
    twoway_string = json.dumps(equities_twoway)
    threeway_string = json.dumps(equities_threeway)
    f = open('equities_2way.json', 'w')
    f.write(twoway_string)
    f.close()
    f = open('equities_3way.json', 'w')
    f.write(threeway_string)
    f.close()

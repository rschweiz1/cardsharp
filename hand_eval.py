
card_values = {'A':14, 'K':13, 'Q':12, 'J':11, 'T':10, '9':9, '8':8, '7':7, '6':6, '5':5, '4':4, '3':3, '2':2}

def evaluate_hand(hand):
    hand.sort(reverse = True, key = lambda x: card_values[x[0]])
    best_hand, rank = check_flush(hand)
    if not best_hand:
        best_hand, rank = check_straight(hand)
        if not best_hand:
            best_hand, rank = check_oak(hand)
    return best_hand, rank


def check_flush(hand):
    suits = [cards[1] for cards in hand]
    suits_mode = max(suits, key = suits.count)
    if suits.count(suits_mode) < 5:
        return False, 0
    else:
        suited_hand = [cards for cards in hand if cards[1] == suits_mode]
        # check for straight flush
        best_hand, rank = check_straight(suited_hand)
        if not best_hand:
            rank = [card_values[suited_hand[i][0]] for i in range(5)]
            return 'flush', rank
        else:
            return 'straight_flush', rank


def check_straight(hand):
    rank = 0
    sequence = 0
    hand.append(hand[0])
    for i in range(1,len(hand)):
        val = card_values[hand[i][0]]
        prev_val = card_values[hand[i-1][0]]
        if (val == (prev_val - 1)) or (val == (prev_val + 12)):
            if not rank:
                rank = [prev_val]
            sequence += 1
            if sequence == 4:
                hand.pop()
                return 'straight', rank
        elif val != prev_val:
            rank = 0
            sequence = 0
    hand.pop()
    return False, 0


def check_oak(hand):
    values = [card_values[cards[0]] for cards in hand]
    values_mode = max(values, key = values.count)
    if values.count(values_mode) == 4:
        kickers = [value for value in values if value != values_mode]
        rank = [values_mode, kickers[0]]
        return 'quads', rank
    elif values.count(values_mode) == 3:
        kickers = [value for value in values if value != values_mode]
        # check for full house
        kickers_mode = max(kickers, key = kickers.count)
        if kickers.count(kickers_mode) >= 2:
            rank = [values_mode, kickers_mode]
            return 'boat', rank
        else:
            rank = [values_mode, kickers[0], kickers[1]]
            return 'trips', rank
    elif values.count(values_mode) == 2:
        kickers = [value for value in values if value != values_mode]
        # check for two pair
        kickers_mode = max(kickers, key = kickers.count)
        if kickers.count(kickers_mode) == 2:
            kickers = [kicker for kicker in kickers if kicker != kickers_mode]
            rank = [values_mode, kickers_mode, kickers[0]]
            return 'two_pair', rank
        else:
            rank = [values_mode, kickers[0], kickers[1], kickers[2]]
            return 'pair', rank
    else:
        rank = values[:5]
        return 'high_card', rank

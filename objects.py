import json
import numpy as np

from random import shuffle
from itertools import product, combinations
from hand_eval import evaluate_hand, card_values

class Dealer:
    def __init__(self):
        self.deck = list(product('AKQJT98765432', 'shdc'))
        self.deck = [''.join(self.deck[i]) for i in range(len(self.deck))]
        self.board = []
        self.hands = [[]]
        self.pot = 0

    def shuffle_deck(self):
        shuffle(self.deck)

    def deal_players(self, numcards, numplayers):
        self.hands = [[self.deck.pop() for _ in range(numcards)] for _ in range(numplayers)]

    def deal_board(self, numcards):
        for _ in range(numcards):
            self.board.append(self.deck.pop())

    def blind_equity(self, player, ways):
        wins = 0
        total = 0
        while total < 2**20:
            cards_remaining = 5 - len(self.board)
            self.shuffle_deck()
            for i in range(ways-1):
                self.hands.append([self.deck[-2*i-1], self.deck[-2*i-2]])
            self.board += [self.deck[i] for i in range(cards_remaining)]
            winner = self.determine_winner()
            if player == winner:
                wins += 1
            elif isinstance(winner, list) and (player in winner):
                wins += 1/len(winner)
            total += 1
            equity = wins/total
            for _ in range(ways-1):
                self.hands.pop()
            self.board = self.board[:(5 - cards_remaining)]
        return equity

    def real_equity(self, player):
        cards_remaining = 5 - len(self.board)
        card_combos = list(combinations(self.deck, cards_remaining))
        outs = 0
        for combo in card_combos:
           self.board += combo
           if player == self.determine_winner():
               outs += 1
           self.board = self.board[:(5 - cards_remaining)]
        equity = outs/len(card_combos)
        print('{:.0%}'.format(equity))
        return equity

    def determine_winner(self):
        # evaluates the best 5-card hand the player can make
        high_hands = [hand_rankings[evaluate_hand(self.hands[i] + self.board)[0]] for i in range(len(self.hands))]
        if high_hands.count(max(high_hands)) == 1:
            # winner found. return from function
            winner = high_hands.index(max(high_hands))
            return winner
        else:
            # go to a tiebreaker
            high_hand_ranks = np.transpose(np.array([evaluate_hand(self.hands[i] + self.board)[1] for i in range(len(high_hands)) if high_hands[i] == max(high_hands)]))
            tied_players =  [players for players in high_hands if players == max(high_hands)]
            for i in range(high_hand_ranks.shape[0]):
                tied_players = [tied_players[j] for j in range(high_hand_ranks.shape[1]) if high_hand_ranks[i][j] == max(high_hand_ranks[i])]
                if len(tied_players) == 1:
                    # winner found. return from function
                    winner = tied_players[0]
                    return winner
                else:
                    high_hand_ranks =  np.array([[high_hand_ranks[k][j] for j in range(high_hand_ranks.shape[1]) if high_hand_ranks[i][j] == max(high_hand_ranks[i])] for k in range(high_hand_ranks.shape[0])])
            winner = tied_players
            return winner

hand_rankings = {'straight_flush':8, 'quads':7, 'boat':6, 'flush':5, 'straight':4, 'trips':3, 'two_pair':2, 'pair':1, 'high_card':0}


class Player():
    def __init__(self, hand, stack):
        self.stacksize = stack
        self.hand = sorted(hand, reverse = True, key = lambda x: card_values[x[0]])

        with open('equities_2way.json', 'r') as f:
            hand_equities_2way = json.load(f)

        with open('equities_3way.json', 'r') as f:
            hand_equities_3way = json.load(f)

        hand_abbv = self.hand[0][0] + self.hand[1][0]
        if self.hand[0][1] == self.hand[1][1]:
            hand_abbv += 's'

        self.preflop_equity_2way = hand_equities_2way[hand_abbv]
        self.preflop_equity_3way = hand_equities_3way[hand_abbv]

    def bet(self, pot, betsize):
        pot += betsize
        self.stacksize -= betsize
        return betsize

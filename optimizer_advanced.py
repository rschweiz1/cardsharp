from objects import Dealer, Player
from pyomo.environ import *
from pyomo.opt import SolverFactory

dealer = Dealer()
dealer.shuffle_deck()
dealer.deal_players(2, 2)

hero = Player(dealer.hands[0], 100)
villan = Player(dealer.hands[1], 100)

hero.bet(dealer.pot, 0.5)
villan.bet(dealer.pot, 1)

print(hero.hand)
print(hero.preflop_equity_2way)


eff_stack = min(hero.stacksize, villan.stacksize)

# Initialize model and constraints
model = ConcreteModel()
model.callmove = Var( initialize = 0, within = Binary )
model.raisemove = Var( initialize = 0, within = Binary )
model.betsize = Var(initialize = 0, within = NonNegativeReals)

model.obj = Objective(expr = model.callmove * (hero.preflop_equity_2way * ( dealer.pot ) + (hero.preflop_equity_2way * 0.5 - 0.5))
                            + model.raisemove * ( hero.preflop_equity_2way * model.betsize - model.betsize )
                            + model.callmove * ( 0.7 * 0 # Villan Checks 70%
                                               + 0.3 * (hero.preflop_equity_2way - 0.1) * (dealer.pot * 2) -  (dealer.pot * 2)) # Villan Bets 30%
                            + model.raisemove * (  0.4 * (1 - hero.preflop_equity_2way) * (dealer.pot + model.betsize) # Villan Folds 40%
                                                 + 0.4 * (hero.preflop_equity_2way - 0.1) * (model.betsize - 0.5)      # Villan Calls 40%
                                                 + 0.2 * ( (hero.preflop_equity_2way - 0.2) * (model.betsize * 3) - 0.2 * ( dealer.pot + model.betsize ) - 2*model.betsize + 0.5) ), # Villan Re-Raises 20%
                             sense = maximize)

model.limits = ConstraintList()

model.limits.add( model.betsize >= model.raisemove * 1.5 )
model.limits.add( model.betsize <= model.raisemove * eff_stack )
model.limits.add( model.callmove + model.raisemove <= 1 )

# Use glpk solver to maximize objective size
opt = SolverFactory('mindtpy')
results = opt.solve(model, mip_solver='glpk', nlp_solver='ipopt')

model.display()

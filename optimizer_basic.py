from objects import Dealer, Player
from pyomo.environ import *
from pyomo.opt import SolverFactory

dealer = Dealer()
dealer.shuffle_deck()
dealer.deal_players(2, 2)

hero = Player(dealer.hands[0], 100)
villan = Player(dealer.hands[1], 100)

hero.bet(dealer.pot, 0.5)
villan.bet(dealer.pot, 1)

print(hero.hand)
print(hero.preflop_equity_2way)


eff_stack = min(hero.stacksize, villan.stacksize)

# Initialize model and constraints
model = ConcreteModel()
model.bet = Var(initialize = 0, within = Binary)
model.betsize = Var(initialize = 0, bounds = (0, eff_stack))

model.obj = Objective(expr = model.bet*(hero.preflop_equity_2way * (2 * model.betsize + 1)) - model.betsize, sense = maximize)

model.limits = ConstraintList()
model.limits.add(model.betsize <= hero.preflop_equity_2way * (model.betsize + 1.5))
model.limits.add(model.betsize >= 1.5 * model.bet)

# Use glpk solver to maximize objective size
opt = SolverFactory('mindtpy')
opt.solve(model, mip_solver='glpk', nlp_solver='ipopt')

model.display()

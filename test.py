from math import floor
from collections import defaultdict

grades = [['Rob', '92.7'], ['Maw', '67'], ['Rob', '83'], ['Maw', '52'], ['Mike', '85'], ['Rob', '0']]

gradekey = defaultdict(list)

for name, grade in grades:
    gradekey[name].append(float(grade))
print(gradekey)

averages = [sum(gradekey[name])/len(gradekey[name]) for name in gradekey]
print(floor(max(averages)))

arr = [-1, -1 , 3, 2, 1]
n = 3
arr = set(arr)
for _ in range(n-1):
    arr.remove(min(arr))
print(min(arr))
